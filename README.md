File Managed Alter
================
This is simple utility module that enables you to increase the length of the
'uri' field used by the 'file_managed' table which is in turn used by Drupal's
'file' entity. This module increases the maximum length from varchar(255) to
varchar(2048).

Background
------------
By default, in Drupal there is a restriction of 255 chars on the varchar size or 
length of the uri field in the file_managed table. In some circumstances this 
is not sufficient and may result in an exception.

e.g.

```ResponseText: The website encountered an unexpected error. Please try again later.Drupal\Core\Entity\EntityStorageException: SQLSTATE[22001]: String data, right truncated: 1406 Data too long for column 'uri' at row 1: INSERT INTO "file_managed" ("uuid", "langcode", "uid", "filename", "uri", "filemime", "filesize", "status", "created", "changed") VALUES (:db_insert_placeholder_0, :db_insert_placeholder_1, :db_insert_placeholder_2, :db_insert_placeholder_3, :db_insert_placeholder_4, :db_insert_placeholder_5, :db_insert_placeholder_6, :db_insert_placeholder_7, :db_insert_placeholder_8, :db_insert_placeholder_9); Array
```

## Reasons to increase the uri length
Microsoft Internet Explorer has a maximum uniform resource locator (URL) length 
of 2,083 characters. Internet Explorer also has a maximum path length of 2,048 
characters.

If you are using the GET method, you are limited to a maximum of 2,048 
characters, minus the number of characters in the actual path.

RFC 2616, "Hypertext Transfer Protocol -- HTTP/1.1," does not specify any 
requirement for URL length.

## Reasons to NOT increase the uri length

Other modules both core and contrib may make assumptions about the length of 
this field.

## Usage
Typically in Drupal, updates to the database schema are allowed when using a 
hook_update. This modules provides a means of doing this one off operation. 
Simply install this module then run the update.php script e.g. `drush updb` or 
if preferred via the admin UI.

Author
-----------
* Daniel Lobo (2dareis2do)

Credit 
-----------
* Ryo Yamashita (yaz)
